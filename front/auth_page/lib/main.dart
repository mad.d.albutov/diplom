import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calendar intagration with UlSTU',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'UlstuIntegrationService'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String _url = "";
  String _text = "";
  String _loadingText = "";
  bool isVisible = false;

  final textController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    textController.dispose();
    super.dispose();
  }

  void setUrlForLink(url) {
    setState(() {
      _url = url;
    });
  }

  void onStartButtonPressed() {
    getCalendarUrl(textController.text);
  }

  void getCalendarUrl(String calName) async {
    try {
      Dio dio = Dio();
      dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        compact: false,
      ));
      var response = await dio
          .get('http://127.0.0.1:8081/service/get/url', queryParameters: {
        "filter": calName,
      });
      Map data = response.data;
      print(data['url']);
      setUrlForLink(data['url']);
      isVisible = true;
    } catch (e, st) {
      print(e);
      print(st);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Введите группу или имя преподавателя',
            ),
            Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(5),
            ),
            TextField(
              obscureText: false,
              controller: textController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Группа или имя',
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(5),
            ),
            FloatingActionButton(
              onPressed: onStartButtonPressed,
              tooltip: 'Получить календарь',
              child: const Icon(Icons.start),
            ),
            Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(5),
            ),
            if (isVisible)
              Center(
                child: InkWell(
                    child: const Text('Ссылка на календарь'),
                    // ignore: deprecated_member_use
                    onTap: () => launch(_url)),
              ),
          ],
        ),
      ),
    );
  }
}
