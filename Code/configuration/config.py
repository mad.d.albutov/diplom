import json
import os

class Config:

    def __init__(self, filename: str) -> None:
        config_file = open(f'{os.path.dirname(__file__)}/{filename}', 'r')
        self.config_json = json.load(config_file)
        self.is_loaded = True
        pass

    def get_calendar_api_scopes(self): # -> ...
        return self.config_json['api']['google']['scope']

    def get_ulstu_api_route(self): # -> ...
        return self.config_json['api']['ulstu']['base_url']

    def get_period_durations(self): # -> ...
        return self.config_json['dates']['first_period']['end']

    def is_loaded(self) -> bool:
        return self.is_loaded


