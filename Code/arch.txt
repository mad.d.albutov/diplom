1. таймтейбл сервис - сервис по преобразованию расписания в гугл календарь
 - инпут: роль юзера (учитель (фио), студент (группа))
 - аутпут: status code
=====
User: Student, Teacher (output) [user.py]

2. vendor lock
- ulstu api wrapper (input: api route from config, output: array of pairs)
- google calendar (input token & calendar id from config, output: response code)
=====
Pair-array [vendor/ulstu_wrapper.py], Calendar routines [vendor/google_calendar.py]

3. calendar event 
- duration (by order)
- summary (aka pair name)
- description (cabinet no., teacher name)
- will be repeated?
=====
Response code [calendar-event.py]

4. config (loads configuration from file (tokens api routes etc)):
- input (relative file name)
- output (status code)
=====
[config.py]

references:
    pip install python-dateutil
    pip install google-api-python-client
    pip install google_auth_oauthlib
    //pip install graphviz
