class Pair:
    week: int
    day: int
    order: int
    name: str
    room: str
    teacher: str

    def __init__(self, week, day, order, name, room, teacher):
        self.week = week
        self.day = day
        self.order = order
        self.name = name
        self.room = room
        self.teacher = teacher