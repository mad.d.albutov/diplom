from calendar import calendar
import json
import os
import urllib

class Storage:
    def __init__(self) -> None:
        self.filename = 'calendars_storage.json'
        self.filepath = f'{os.path.dirname(__file__)}/{self.filename}'
        storage_file = open(self.filepath, 'r')
        self.storage_json = json.load(storage_file)
        pass

    def save_calendar(self, name, id, hash, url):
        new_json = self.storage_json
        if not self.is_contains(name):
            new_json['calendars'].append({"name": name, "id": id, "hash": hash, "url": url})
            print(f"Calendar [{name}] saved")


        with open(self.filepath, 'w') as f:
            json.dump(new_json, f)

    def is_contains(self, key) -> bool:
        for elem in self.storage_json['calendars']:
            if elem['name'] == key:
                return True
        
        return False

    def update_hash(self, cal_id, hash):
        new_json = self.storage_json

        for cal in new_json['calendars']:
            if cal['id'] == cal_id:
                cal['hash'] = hash

        with open(self.filepath, 'w') as f:
            json.dump(new_json, f)

    def load_calendars(self):
        return self.storage_json


