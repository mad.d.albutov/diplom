


import requests
from entities.pair import Pair
import json
import hashlib
LOCAL_API_URL = 'http://localhost:8080/api/1.0/timetable'

class UlstuApi(object):    
    
    def __init__(self, base_url: str) -> None:
        self.domain = base_url

    def get_pairs_by_filter(self, filter):

        query_param = '?filter=' + str(filter)
        api_path = '/api/1.0/timetable'
        request_url = self.domain + api_path + query_param

        response = requests.get(LOCAL_API_URL)
        timetable = response.json()['response'] 
        a = json.dumps(timetable, sort_keys=True).encode("utf-8")
        print(a)
        hash = hashlib.md5(a).hexdigest()
        pairArr = []

        for weekId, week in enumerate(timetable['weeks']): 
            for dayId, day in enumerate(week['days']):
                for pairId, pair in enumerate(day['lessons']):        
                    if pair:    
                        for part in pair:                   
                            pairArr.append(
                                Pair(
                                    week=weekId, 
                                    day=dayId,
                                    order=pairId, 
                                    name=part['nameOfLesson'], 
                                    room=part['room'], 
                                    teacher=part['teacher']
                                )
                            )
                            print(f"{part['nameOfLesson']}")
        return pairArr, hash