import os
import googleapiclient
import os.path
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
import time
from configuration.config import Config

class GoogleCalendar(object):
    def __init__(self):
        dirname = os.path.dirname(__file__)
        token_path = f'{dirname}/../auth_credentials/token.json'
        cred_path = f'{dirname}/../auth_credentials/credentials.json'

        creds = None
        config = Config('config.json')
        if os.path.exists(token_path): 
            creds = Credentials.from_authorized_user_file(token_path, [config.get_calendar_api_scopes()])

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(cred_path, config.get_calendar_api_scopes())
                creds = flow.run_local_server(port=8080)

            with open(token_path, 'w') as token:
                token.write(creds.to_json())

        self.service = googleapiclient.discovery.build('calendar', 'v3', credentials=creds)

    def get_calendar_id_by_name(self, name):
        calendars = self.get_calendar_list()
        target_id = None
        for c in calendars:
            if c['summary'] == name:
                target_id = c['id']
        
        return target_id

    def add_calendar_public_rule(self, calendar_id):
        acl = self.service.acl()
 
        body = {
            'role': 'reader',
            'scope': {
                'type': 'default',
                'value': '__public_principal__@public.calendar.google.com'
            }
        }
 
        acl.insert(
            calendarId=calendar_id, 
            body=body
        ).execute()

    def create_calendar(self, title):
        calendar = {
            "summary" : title,
            "timeZone" : 'Europe/Samara'
        }
        c = self.service.calendars().insert(body=calendar).execute()
        print(f"Calendar created with id: {c['id']}")
        print(f"Making calendar [{c['summary']}] to public visibility ...")
        self.add_calendar_public_rule(c['id'])
        print(f"Calendar {c['summary']} now is public")
        return c['id']

    def get_calendar_url(self, calendar_id):
        incoded_id = calendar_id.replace('@', '%40', 1)
        left_part = 'https://calendar.google.com/calendar/ical/'
        right_part = '/public/basic.ics'
        print(f"Url created [{left_part + incoded_id + right_part}]")
        return left_part + incoded_id + right_part

    def delete_calendar(self, calendar_id):
        print('Deleting calendar with id: ' + calendar_id + ' ...')
        self.service.calendars().delete(calendarId=calendar_id).execute()
        print('Calendar was deleted.')

    def get_calendar_list(self):
        print('Getting calendars list...')
        page_token = None
        result_calendars = []
        while True:
            calendar_list_page = self.service.calendarList().list(maxResults=5,pageToken=page_token).execute()
            for calendar in calendar_list_page['items']: 
                result_calendars.append(calendar)
                print('Recived calendar: ')
                print(f"\t id: {calendar['id']}")
                print(f"\t name: [{calendar['summary']}]")
                print('_________________________________')
            page_token = calendar_list_page.get('nextPageToken')
            if not page_token:
                break
        return result_calendars

    # создание события в календаре
    def create_event(self, calendar_id, event):
        print('Creating event with name: ' + event['summary'])

        time.sleep(0.300)
        e = self.service.events().insert(calendarId=calendar_id,
                                         body=event).execute()
        print(f"Event created: {e.get('id')}") 

    def delete_all_events(self, calendar_id):
        for event in self.get_events_list(calendar_id):
            print('Deleting event with name: ' + event['summary'])
            time.sleep(0.300)
            self.service.events().delete(calendarId=calendar_id, eventId=event['id']).execute()
            print(event['summary'] + ' was deleted')

    def get_events_list(self, calendar_id):
        print('Getting event list for calendar id: ' + calendar_id)

        result_event_array=  []
        page_token = None

        while True:
            events_list_page = self.service.events().list(
                calendarId=calendar_id, 
                pageToken=page_token).execute()
                
            for event in events_list_page['items']:
                print('Event recived with name: ' + event['summary'])
                result_event_array.append(event)
                page_token = events_list_page.get('nextPageToken')

            if not page_token:
                break

        return result_event_array

    def get_acl_list(self, calendar_id):
        a = self.service.acl().list(calendarId=calendar_id).execute()
        for item in a['items']:
            print(item)
