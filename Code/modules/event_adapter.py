
from entities.event_datetime_constants import *
from entities.pair import *
import math
from dateutil.relativedelta import relativedelta, MO
from datetime import datetime, timedelta

PairList = list[Pair]

class EventAdapter:    

    def __init__(self, period_start=0, period_end=0) -> None:
        self.period_start = None
        self.period_end = None
        pass

    def get_start_date_by_week_order(self, week_order) -> datetime:
        if week_order == self.get_current_week_order():
            return datetime.now() + relativedelta(weekday=MO(-1))
        else:
            return (datetime.now() + relativedelta(weekday=MO(-1))) + timedelta(days=7)

    def parse_pairs_to_event(self, pairs: PairList) -> list:
        events = []
        for pair in pairs:
            events.append(self.create_event_from_pair(pair))
        return events

    def get_current_week_order(self, offset=0):
        current_date = datetime.now() + timedelta(days=offset) + timedelta(days=-3)
        current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
        first_jan = datetime(year=current_date.year, month=1, day=1)
        order = math.ceil((((current_date.timestamp() * 1000 - first_jan.timestamp() * 1000) / 86400000) + first_jan.day + 1) / 7) % 2

        return order



    def create_event_from_pair(self, pair: Pair):
        start_date = self.get_start_date_by_week_order(pair.week).date()
        start_date = start_date + timedelta(days=(pair.day))
        start_time = pair_order[pair.order]['start']
        end_time = pair_order[pair.order]['end']
        day = weekday[pair.day]
        summary = f'{pair.name} \n {pair.room}'
        description = pair.teacher

        event = {
            'summary': summary,
            'description': description,
            'start': {
                'dateTime': f'{start_date}T{start_time}',
                'timeZone': 'Europe/Samara'
            },
            'end': {
                'dateTime': f'{start_date}T{end_time}',
                'timeZone': 'Europe/Samara'
            },
            'recurrence': [
                f'RRULE:FREQ=WEEKLY;INTERVAL=2;BYDAY={day};UNTIL=20220901T170000Z'
            ]
        }

        return event
