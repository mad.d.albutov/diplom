from __future__ import print_function
import urllib.parse
import flask
from flask import request, json

from google_api_wrapper import *
from ulstu_api_wrapper import UlstuApi
from configuration.config import Config
from event_adapter import EventAdapter
import time
from flask import Flask
import time
from storage.storage_manager import Storage

# https://calendar.google.com/calendar/ical/0484nes4bmnsacmi7rj111vaa8%40group.calendar.google.com/public/basic.ics


app = Flask(__name__)


class CoreController:

    def __init__(self) -> None:
        config = Config("config.json")
        self.google_cal = GoogleCalendar()
        self.ulstu_api = UlstuApi(base_url=config.get_ulstu_api_route())
        self.adapter = EventAdapter()
        pass

    def create_calendar(self, calendar_name):
        decoded_name = urllib.parse.unquote(calendar_name)
        store = Storage()

        if store.is_contains(calendar_name):
            print(f"Calendar [{calendar_name}] already exists")
            for e in store.load_calendars()['calendars']:
                if e['name'] == calendar_name:
                    return e['url']


        pairs, hash = self.ulstu_api.get_pairs_by_filter(filter)

        events = self.adapter.parse_pairs_to_event(pairs)

        cal_id = self.google_cal.create_calendar(calendar_name)
        for event in events:
            self.google_cal.create_event(cal_id, event)
            time.sleep(3)
        url = self.google_cal.get_calendar_url(cal_id)
        store.save_calendar(calendar_name, cal_id, hash, url)

        return url




@app.route('/service/get/url', methods=["GET", "POST"])
def get_urls():
    filter = request.args.get('filter')
    response = None
    controller = CoreController()
    try:
        calendar_url = controller.create_calendar(filter)
        data = {
            "url": calendar_url
        }
        response = flask.jsonify(data)
        response.headers.add('Access-Control-Allow-Origin', '*')
    except:
        print('Error creating calendar')
        response = {"url": "error"}
    return response


@app.route('/service/check/relevance', methods=["GET", "POST"])
def check_rel():
    print('Checking relevance...')
    controller = CoreController()
    storeage = Storage()
    
    storage_cal = storeage.load_calendars()['calendars']
    for cal in storage_cal:
        time.sleep(0.200)
        pairs, hash = controller.ulstu_api.get_pairs_by_filter(cal['name'])
        if not hash == cal['hash']:
            print(f"Data of calendar [{cal['name']}] is rushed ")
            controller.google_cal.delete_all_events(cal['id'])
            events = controller.adapter.parse_pairs_to_event(pairs=pairs)
            for e in events:
                controller.google_cal.create_event(cal['id'], e)
            storeage.update_hash(cal['id'], hash)

    print('Checking relevance completed')

    return "<p>Checking completed!</p>"



    
    


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8081, debug=True)

