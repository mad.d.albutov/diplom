from flask import Flask

app = Flask(__name__)




@app.route("/api/1.0/timetable")
def get_timetable():
    body = {
        "response": {
            "weeks": [
                {
                    "days": [
                        {
                            "day": 0,
                            "lessons": [
                                [
                                    {
                                        "group": "Тест группа",
                                        "nameOfLesson": "ОБЖ",
                                        "room": "044",
                                        "teacher": "Албутов"
                                    }
                                ],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 1,
                            "lessons": [
                                [],
                                [],
                                [],
                                [
                                    # {
                                    #     "group": "Тест группа",
                                    #     "nameOfLesson": "Русский",
                                    #     "room": "044",
                                    #     "teacher": "Албутов"
                                    # }
                                ],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 2,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 3,
                            "lessons": [
                                [],
                                [],
                                [
                                    # {
                                    #     "group": "Тест группа",
                                    #     "nameOfLesson": "Математика",
                                    #     "room": "044",
                                    #     "teacher": "Албутов"
                                    # }
                                ],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 4,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 5,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        }
                    ]
                },
                {
                    "days": [
                        {
                            "day": 0,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 1,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 2,
                            "lessons": [
                                [],
                                [],
                                [
                                    # {
                                    #     "group": "Тест группа",
                                    #     "nameOfLesson": "Русский",
                                    #     "room": "044",
                                    #     "teacher": "Албутов"
                                    # }
                                ],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 3,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [
                                    # {
                                    #     "group": "Тест группа",
                                    #     "nameOfLesson": "Программирование",
                                    #     "room": "044",
                                    #     "teacher": "Албутов"
                                    # }
                                ],
                                [
                                    {
                                        "group": "Тест группа",
                                        "nameOfLesson": "Физра",
                                        "room": "044",
                                        "teacher": "Албутов"
                                    }
                                ],
                                []
                            ]
                        },
                        {
                            "day": 4,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                [],
                                []
                            ]
                        },
                        {
                            "day": 5,
                            "lessons": [
                                [],
                                [],
                                [],
                                [],
                                [
                                    # {
                                    #     "group": "Какая-то группа",
                                    #     "nameOfLesson": "Электротехника",
                                    #     "room": "044",
                                    #     "teacher": "Албутов"
                                    # }
                                ],
                                [],
                                [],
                                []
                            ]
                        }
                    ]
                }
            ]
        },
        "error": ""
    }
    return body

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8080, debug=True)
